#!/usr/bin/env ts-node
class Customer {
  name: String;

  sayName() {
    return `My name is ${this.name}`;
  }
}

const peteAgain = new Customer();
peteAgain.name = 'Pete';

console.log(`Hello ${peteAgain.sayName()}`);
